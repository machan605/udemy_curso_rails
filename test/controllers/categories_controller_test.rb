require 'test_helper'

class CategoriesControllerTest < ActionController::TestCase
 def setup
   @category = Category.create(name: "sports")
   @user = User.create(username: "julio", email: "julio@gmail.com", password: "password", admin: true)
 end

  test "debe tener cateroias index" do
    get :index
    assert_response :success
  end

  test "debe tener new" do
    session[:user_id] = @user.id
    get :new
    assert_response :success
  end

  test "debe tener show" do
    get( :show, {'id' => @category.id})
    assert_response :success
  end

  test "debe redirigir cuan el admin no esta log" do
    assert_no_difference 'Category.count' do
      post :create, category: { name: "sports" }
    end
    assert_redirected_to categories_path
  end


end
